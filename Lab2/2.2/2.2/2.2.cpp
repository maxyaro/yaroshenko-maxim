// 2_2.cpp : Defines the entry point for the console application. 
// 

#include "stdafx.h" 
#include <iostream> 
using namespace std; 


template <class max> 
max Get_max(max mass[], int Size) { 
max maximum = mass[0]; 
for (int i = 0; i < Size; i++) { 
if (mass[i] > maximum) { 
maximum = mass[i]; 
} 

} 
return maximum; 
} 

int main() 
{ 
int max_mass[] = {4, 2, 6, 8}; 
cout << Get_max<int>(max_mass, 4)<< endl; 

system ("pause"); 
return 0; 

}