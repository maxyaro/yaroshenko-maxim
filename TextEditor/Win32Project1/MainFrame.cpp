#include <wx/wx.h>
#include "MainFrame.h"



MainFrame::MainFrame(const wxString& title, const wxPoint& pos, const wxSize& size)
	:wxFrame(NULL, -1, title, pos, size)
{
	wxButton* sampleButton = new wxButton(this, 0, "click me");
}

void MainFrame::OnButtonClick(wxCommandEvent& commandEvent)
{
	wxFrame* frame = new wxFrame(this, 2, "This is child window");
	frame->Show(true);
}

BEGIN_EVENT_TABLE(MainFrame, wxFrame)
EVT_BUTTON(0, MainFrame::OnButtonClick)
END_EVENT_TABLE()


