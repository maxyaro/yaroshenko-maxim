#pragma once
#include <wx/wx.h>

class MainFrame : public wxFrame
{
public:
	MainFrame(const wxString& title, const wxPoint& pos, const wxSize& size);
	wxButton* sampleButton;
	void OnButtonClick(wxCommandEvent& CommandEvent);

	DECLARE_EVENT_TABLE()


};