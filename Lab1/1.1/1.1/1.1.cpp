// 1.1.cpp: определяет точку входа для консольного приложения.
//
#include "stdafx.h" 
#include <iostream> 
#include <string> 
using namespace std;

class History {
private:
	int itr = 0;
	string arr[3];
public:
	void print(string text) {
		arr[itr] = text;
		cout << text << endl;
		itr++;
	}

	void ShowHistory() {
		for (int i = 0; i < 3; i++) {
			cout << arr[i] << endl;
		}
	}
};

int main()
{
	History user;
	string user_text;
	cin >> user_text;
	user.print(user_text);
	user.print("Cat and Dog");
	user.ShowHistory();


	system("pause");
	return 0;
}
