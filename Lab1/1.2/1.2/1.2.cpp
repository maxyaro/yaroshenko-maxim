// 1.2.cpp: определяет точку входа для консольного приложения.
//
#include "stdafx.h" 
#include <iostream> 
#include <cmath> 

class Vector
{
private:
	int A, B;

public:
	void SetA(int a) {
		this->A = a;
	}

	void SetB(int b) {
		this->B = b;
	}

	int GetA() {
		return A;
	}

	int GetB() {
		return B;
	}

	double Length() {
		double length = sqrt(pow(A, 2) + pow(B, 2));

		return length;
	}
};



int main()
{
	Vector vec;
	vec.SetA(2);
	vec.SetB(3);
	std::cout << vec.Length(); // вывод ответа в консоль 
	std::cout << vec.GetA();
	system("pause");
	return 0;
}