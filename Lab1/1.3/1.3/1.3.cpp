// 1.3.cpp: определяет точку входа для консольного приложения.
//
#include "stdafx.h" 
#include <string> 
#include <iostream> 
using namespace std;


class BoxStr {
	string stroka;

public:
	void SetValue(string value) {
		this->stroka = value;
	}

	string GetValue() {
		return stroka;
	}

	int SearchChar(char symbol) {
		int povt = 0;
		for (int i = 0; i < stroka.length(); i++) {
			if (symbol == stroka[i])
				povt++;
		}
		return povt;
	}
};

int main()
{
	BoxStr Stroka, Obj;
	Obj.SetValue("Sunny");
	cout << Obj.SearchChar('n') << endl;
	Stroka.SetValue("string value");
	cout << Stroka.GetValue() << endl;
	cout << Stroka.SearchChar('r') << endl;
	system("pause");

	return 0;
}
