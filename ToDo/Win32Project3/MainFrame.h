#pragma once
#include <wx/wx.h>
#include <wx/datectrl.h>
#include <wx/listctrl.h>

class MainFrame : public wxFrame
{
public:
	MainFrame(wxString title, wxPoint position, wxSize size);
	wxBoxSizer* rootSizer;
	wxBoxSizer* todoSizer;

	wxTextCtrl* todoTextCtrl;
	wxDatePickerCtrl* todoDatePickerCtrl;
	wxButton* todoAdd;
	wxButton* todotoday;
	wxDateTime* todoDateTimeCtrl;

	wxListCtrl* todoList;

	wxButton* todoComplete;

	int todoid = 0;

	void onTodoAddClick(wxCommandEvent& event);

	void onTodoCompleteClick(wxCommandEvent& event);

	void onTodotodayClick(wxCommandEvent& event);

	DECLARE_EVENT_TABLE();
};


