#include "MainFrame.h"

enum id {
	todoAddId = wxID_HIGHEST + 1,
	todoComleteId = wxID_HIGHEST + 2,
	todotodayId = wxID_HIGHEST + 3,
};

MainFrame::MainFrame(wxString title, wxPoint position, wxSize size)
	: wxFrame(NULL, wxID_ANY, title, position, size)
{
	rootSizer = new wxBoxSizer(wxVERTICAL);
	todoSizer = new wxBoxSizer(wxHORIZONTAL);

	todoTextCtrl = new wxTextCtrl(this, wxID_ANY);
	todoDatePickerCtrl = new wxDatePickerCtrl(this, wxID_ANY);
	todoAdd = new wxButton(this, todoAddId, "Add");
	todotoday = new wxButton(this, todotodayId, "Today");

	todoSizer->Add(todoTextCtrl, wxEXPAND);
	todoSizer->Add(todoDatePickerCtrl);
	todoSizer->Add(todotoday);
	todoSizer->Add(todoAdd);
	

	todoList = new wxListCtrl(this, wxID_ANY);
	todoComplete = new wxButton(this, todoComleteId, "Complete");


	rootSizer->Add(todoSizer, 0, wxEXPAND);
	rootSizer->Add(todoList, 1, wxEXPAND);
	rootSizer->Add(todoComplete, 0, wxEXPAND);
	SetSizer(rootSizer);
}

void MainFrame::onTodoAddClick(wxCommandEvent& event)
{
	wxString todoText = todoTextCtrl->GetValue();

	if (todoText.IsEmpty() || todoText.Contains(" "))
	{
		wxLogMessage("Todo cannot be empty");
		return;
	}

	wxDateTime todoDate = todoDatePickerCtrl->GetValue();


	wxString todoDateString = todoDate.Format("%y %m %d");
	todoText.Append(todoDateString);


	wxListItem todoItem;
	todoItem.SetText(todoText);
	todoItem.SetId(todoid);


	todoList->InsertItem(todoItem);
	todoid++;
}

void MainFrame::onTodoCompleteClick(wxCommandEvent& event)
{
	long item = -1;
	for (;;)
	{
		item = todoList->GetNextItem(item, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
		if (item == -1)
			break;

		todoList->DeleteItem(item);
	}
}

void MainFrame::onTodotodayClick(wxCommandEvent & event)
{
	wxString todoText = todoTextCtrl->GetValue();

	if (todoText.IsEmpty() || todoText.Contains(" "))
	{
		wxLogMessage("Todo cannot be empty");
		return;
	}

	

	wxDateTime::Now();
	wxDateTime todoDate = todoDatePickerCtrl->GetValue();


	wxString todoDateString = todoDate.Format("%y %m %d");
	todoText.Append(todoDateString);


	wxListItem todoItem;
	todoItem.SetText(todoText);
	todoItem.SetId(todoid);


	todoList->InsertItem(todoItem);
	todoid++;
}



BEGIN_EVENT_TABLE(MainFrame, wxFrame)
EVT_BUTTON(todoAddId, MainFrame::onTodoAddClick)
EVT_BUTTON(todoComleteId, MainFrame::onTodoCompleteClick)
EVT_BUTTON(todotodayId, MainFrame::onTodotodayClick)
END_EVENT_TABLE()
