#include "ToDo.h"
#include "MainFrame.h"


bool ToDo::OnInit()
{
	MainFrame* mainFrame = new MainFrame("ToDo", wxDefaultPosition, wxDefaultSize);
	mainFrame->Show(true);
	SetTopWindow(mainFrame);
	return true;
}

wxIMPLEMENT_APP(ToDo);