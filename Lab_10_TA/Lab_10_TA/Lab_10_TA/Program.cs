﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class JohnsTotter
{

    private static bool LEFT_TO_RIGHT = true;
    private static bool RIGHT_TO_LEFT = false;

    public static int searchArr(int[] a, int n,
                                    int mobile)
    {
        for (int i = 0; i < n; i++)

            if (a[i] == mobile)
                return i + 1;

        return 0;
    }

    public static int getMobile(int[] a,
                   bool[] dir, int n)
    {
        int mobile_prev = 0, mobile = 0;

        for (int i = 0; i < n; i++)
        {
            if (dir[a[i] - 1] == RIGHT_TO_LEFT &&
                                          i != 0)
            {
                if (a[i] > a[i - 1] &&
                            a[i] > mobile_prev)
                {
                    mobile = a[i];
                    mobile_prev = mobile;
                }
            }

            if (dir[a[i] - 1] == LEFT_TO_RIGHT && i != n - 1)
            {
                if (a[i] > a[i + 1] &&
                            a[i] > mobile_prev)
                {
                    mobile = a[i];
                    mobile_prev = mobile;
                }
            }
        }

        if (mobile == 0 && mobile_prev == 0)
            return 0;
        else
            return mobile;
    }

    public static int printOnePerm(int[] a, bool[] dir,
                                                    int n)
    {
        int mobile = getMobile(a, dir, n);
        int pos = searchArr(a, n, mobile);

        if (dir[a[pos - 1] - 1] == RIGHT_TO_LEFT)
        {
            int temp = a[pos - 1];
            a[pos - 1] = a[pos - 2];
            a[pos - 2] = temp;
        }
        else if (dir[a[pos - 1] - 1] == LEFT_TO_RIGHT)
        {
            int temp = a[pos];
            a[pos] = a[pos - 1];
            a[pos - 1] = temp;
        }

        for (int i = 0; i < n; i++)
        {
            if (a[i] > mobile)
            {
                if (dir[a[i] - 1] == LEFT_TO_RIGHT)
                    dir[a[i] - 1] = RIGHT_TO_LEFT;

                else if (dir[a[i] - 1] == RIGHT_TO_LEFT)
                    dir[a[i] - 1] = LEFT_TO_RIGHT;
            }
        }

        for (int i = 0; i < n; i++)
            Console.Write(a[i]);

        Console.Write(" ");

        return 0;
    }

    public static int fact(int n)
    {
        int res = 1;

        for (int i = 1; i <= n; i++)
            res = res * i;
        return res;
    }

    public static void printPermutation(int n)
    {
        int[] a = new int[n];

        bool[] dir = new bool[n];

        for (int i = 0; i < n; i++)
        {
            a[i] = i + 1;
            Console.Write(a[i]);
        }

        Console.Write("\n");

        for (int i = 0; i < n; i++)
            dir[i] = RIGHT_TO_LEFT;

        for (int i = 1; i < fact(n); i++)
            printOnePerm(a, dir, n);
    }
}