﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[10];
            int temp;
            int counter = 0;
            Random random = new Random();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = random.Next(0, 100);
                Console.Write(array[i] + ",");
            }
            for (int i = 0; i < array.Length; i++)
            {
                for (int k = i + 1; k < array.Length; k++)
                {
                    if (array[k] < array[i])
                    {
                        temp = array[i];
                        array[i] = array[k];
                        array[k] = temp;
                    }
                    Console.Write(" " + ++counter);
                }
            }
            Console.WriteLine("");
            for (int i = 0; i < array.Length; i++)
                Console.Write(array[i] + ",");

            Console.ReadKey();

        }
    }
}
