#include "MainFrame.h"

enum id {
	displayId = wxID_ANY + 10,
	oneButtonId = wxID_HIGHEST + 2,
	twoButtonId = wxID_HIGHEST + 3,
	threeButtonId = wxID_HIGHEST + 8,
	minusButtonId = wxID_HIGHEST + 9,
	plusButtonId = wxID_HIGHEST + 4,
	equlButtonId = wxID_HIGHEST + 5,
	clsButtonId = wxID_HIGHEST + 6,
	divisionButtonid = wxID_HIGHEST + 7
};


MainFrame::MainFrame(const wxString& title, const wxPoint& pos, const wxSize& size)
	: wxFrame(NULL, wxID_ANY, title, pos, size)
{
	firstOperand = new wxString(wxEmptyString);
	secondOperand = new wxString(wxEmptyString);
	calcOperator = new wxString(wxEmptyString);
	isSecondOperator = false;


	vBoxSizer = new wxBoxSizer(wxVERTICAL);
	display = new wxTextCtrl(this, -1, wxT(""), wxDefaultPosition,
		wxDefaultSize, wxTE_RIGHT | wxTE_READONLY);

	vBoxSizer->Add(display, 0, wxEXPAND | wxTOP | wxBOTTOM, 4);
	gridSizer = new wxGridSizer(5, 4, 3, 3);

	gridSizer->Add(new wxButton(this, clsButtonId, wxT("Cls")), 0, wxEXPAND);
	gridSizer->Add(new wxButton(this, -1, wxT("Bck")), 0, wxEXPAND);
	gridSizer->Add(new wxStaticText(this, -1, wxT("")), 0, wxEXPAND);
	gridSizer->Add(new wxButton(this, -1, wxT("Close")), 0, wxEXPAND);
	gridSizer->Add(new wxButton(this, -1, wxT("7")), 0, wxEXPAND);
	gridSizer->Add(new wxButton(this, -1, wxT("8")), 0, wxEXPAND);
	gridSizer->Add(new wxButton(this, -1, wxT("9")), 0, wxEXPAND);
	gridSizer->Add(new wxButton(this, divisionButtonid, wxT("/")), 0, wxEXPAND);
	gridSizer->Add(new wxButton(this, -1, wxT("4")), 0, wxEXPAND);
	gridSizer->Add(new wxButton(this, -1, wxT("5")), 0, wxEXPAND);
	gridSizer->Add(new wxButton(this, -1, wxT("6")), 0, wxEXPAND);
	gridSizer->Add(new wxButton(this, -1, wxT("*")), 0, wxEXPAND);
	gridSizer->Add(new wxButton(this, oneButtonId, wxT("1")), 0, wxEXPAND);
	gridSizer->Add(new wxButton(this, twoButtonId, wxT("2")), 0, wxEXPAND);
	gridSizer->Add(new wxButton(this, threeButtonId, wxT("3")), 0, wxEXPAND);
	gridSizer->Add(new wxButton(this, minusButtonId, wxT("-")), 0, wxEXPAND);
	gridSizer->Add(new wxButton(this, -1, wxT("0")), 0, wxEXPAND);
	gridSizer->Add(new wxButton(this, -1, wxT(".")), 0, wxEXPAND);
	gridSizer->Add(new wxButton(this, equlButtonId, wxT("=")), 0, wxEXPAND);
	gridSizer->Add(new wxButton(this, plusButtonId, wxT("+")), 0, wxEXPAND);

	vBoxSizer->Add(gridSizer, 1, wxEXPAND);
	SetSizer(vBoxSizer);
	SetMinSize(wxSize(270, 220));

	Centre();
}

void MainFrame::OnOneButtonClick(wxCommandEvent& commandEvent)
{
	display->AppendText("1");

	if (!isSecondOperator) {
		firstOperand->Append("1");
	}
	else {
		secondOperand->Append("1");
	}
}

void MainFrame::OnTwoButtonClick(wxCommandEvent& commandEvent)
{
	display->AppendText("2");

	if (!isSecondOperator) {
		firstOperand->Append("2");
	}
	else {
		secondOperand->Append("2");
	}
}
void MainFrame::OnThreeButtonClick(wxCommandEvent& commandEvent)
{
	display->AppendText("3");

	if (!isSecondOperator) {
		firstOperand->Append("3");
	}
	else {
		secondOperand->Append("3");
	}
}
void MainFrame::OnMinusButtonClick(wxCommandEvent& commandEvent)
{
	display->AppendText("-");
	calcOperator->Empty();
	calcOperator->Append("-");
	isSecondOperator = true;
}
void MainFrame::OnPlusButtonClick(wxCommandEvent& commandEvent)
{
	display->AppendText("+");
	calcOperator->Empty();
	calcOperator->Append("+");
	isSecondOperator = true;
}

void MainFrame::OnEqualButtonClick(wxCommandEvent& commandEvent)
{
	if (calcOperator->ToStdString() == "+")
	{
		long value1;
		this->firstOperand->ToLong(&value1);

		long value2;
		this->secondOperand->ToLong(&value2);

		long res = value1 + value2;


		display->Clear();
		display->AppendText(wxString::Format("%i", res));
	}
	else if (calcOperator->ToStdString() == "/") {
		double value1;
		this->firstOperand->ToDouble(&value1);

		double value2;
		this->secondOperand->ToDouble(&value2);

		double res = value1 / value2;

		display->Clear();
		display->AppendText(wxString::Format("%lf", res));
	}

	{
	else if (calcOperator->ToStdString() == "-") {
		double value1;
		this->firstOperand->ToDouble(&value1);

		double value2;
		this->secondOperand->ToDouble(&value2);

		double res = value1 - value2;

		display->Clear();
		display->AppendText(wxString::Format("%lf", res));
}

void MainFrame::OnClsButtonClick(wxCommandEvent& command)
{
	firstOperand->Empty();
	secondOperand->Empty();
	calcOperator->Empty();
	isSecondOperator = false;
	display->Clear();
}

void MainFrame::OnDivisionButtonClick(wxCommandEvent& command)
{
	display->AppendText("/");
	calcOperator->Empty();
	calcOperator->Append("/");
	isSecondOperator = true;
}

BEGIN_EVENT_TABLE(MainFrame, wxFrame)
EVT_BUTTON(oneButtonId, MainFrame::OnOneButtonClick)
EVT_BUTTON(twoButtonId, MainFrame::OnTwoButtonClick)
EVT_BUTTON(plusButtonId, MainFrame::OnPlusButtonClick)
EVT_BUTTON(equlButtonId, MainFrame::OnEqualButtonClick)
EVT_BUTTON(clsButtonId, MainFrame::OnClsButtonClick)
EVT_BUTTON(divisionButtonid, MainFrame::OnDivisionButtonClick)
EVT_BUTTON(threeButtonId, MainFrame::OnThreeButtonClick)
EVT_BUTTON(minusButtonId, MainFrame::OnMinusButtonClick)
END_EVENT_TABLE()
