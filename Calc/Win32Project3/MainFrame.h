#pragma once
#include <wx/wx.h>

class MainFrame : public wxFrame
{
public:
	MainFrame(const wxString& title, const wxPoint& pos, const wxSize& size);

	wxTextCtrl* display;
	wxBoxSizer* vBoxSizer;
	wxGridSizer* gridSizer;
	wxString* firstOperand;
	wxString* calcOperator;
	wxString* secondOperand;
	bool isSecondOperator;

	void OnOneButtonClick(wxCommandEvent& commandEvent);
	void OnTwoButtonClick(wxCommandEvent& commandEvent);
	void OnThreeButtonClick(wxCommandEvent& commandEvent);
	void OnPlusButtonClick(wxCommandEvent& commandEvent);
	void OnMinusButtonClick(wxCommandEvent& commandEvent);
	void OnEqualButtonClick(wxCommandEvent& commandEvent);
	void OnClsButtonClick(wxCommandEvent& commandEvent);
	void OnDivisionButtonClick(wxCommandEvent& command);


	DECLARE_EVENT_TABLE();


};