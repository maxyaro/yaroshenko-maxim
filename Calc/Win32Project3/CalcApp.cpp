#include "CalcApp.h"

bool CalcApp::OnInit()
{
	MainFrame* mainFrame = new MainFrame("CalcApp", wxDefaultPosition, wxDefaultSize);
	SetTopWindow(mainFrame);
	mainFrame->Show(true);

	return true;
}

wxIMPLEMENT_APP(CalcApp);