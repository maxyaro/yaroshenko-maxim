﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearSearch

{
    class Program
    {
        static int[] a = new int[1000];
        static int counter; 
        static int n;   
        static void Main(string[] args)
        {
            Console.Write("Ввести количество элементов: ");
            n = Convert.ToInt32(Console.ReadLine());
               
            Console.WriteLine("\nПоиск в неупорядоченном массиве");
            arrGen();
            comparison();
            Console.ReadKey();
        }
       
        static void comparison()
        {
             
            Console.Write("Ввести элемент поиска: ");
            int b = Convert.ToInt32(Console.ReadLine());
           
            int k = linePoisk(b);
            Console.WriteLine("Линейный поиск:");
            if (k > -1)
                Console.WriteLine("Номер элемента = {0}, число сравнений внутри цикла = {1}", k, counter);
            else
                Console.WriteLine("нет заданного элемента, число сравнений внутри цикла = {0}", counter);
        }
       
        static void arrGen()
        {
            Random ran = new Random();
            for (int i = 0; i < n; i++)
            {
                a[i] = ran.Next(1, 899);
                Console.Write("  {0}", a[i]);
            }
            Console.WriteLine();
        }
           
        static int linePoisk(int b)
        {
            int k = -1;
            counter = 0;
            for (int i = 0; i < n; i++)
            {
                counter++;
                if (a[i] == b) { k = i; break; };
            }
            return k;
        }
    }
}