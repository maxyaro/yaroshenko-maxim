#include "AboutFrame.h"



AboutFrame::AboutFrame(const wxString& title)
	:wxFrame(NULL, wxID_ANY, title, wxDefaultPosition, wxDefaultSize) 
{
	wxImage::AddHandler(new wxPNGHandler);
	
	wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
	wxBitmap bmp("C:/Users/User/source/repos/Laba6/image/page.png", wxBITMAP_TYPE_PNG);
	wxStaticBitmap *sb = new wxStaticBitmap(this, -1, bmp);

	sizer->Add(sb, 1, wxEXPAND);

	int width = bmp.GetWidth();
	int height = bmp.GetHeight();
	SetMinSize(wxSize(width, height));

	Center();
}