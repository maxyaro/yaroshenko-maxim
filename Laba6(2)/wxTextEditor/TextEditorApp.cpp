#include "TextEditorApp.h"
wxIMPLEMENT_APP(TextEditorApp);



bool TextEditorApp::OnInit() {
	MainFrame* mainFrame = new MainFrame("BestTextEditor", wxDefaultPosition, wxDefaultSize);
	SetTopWindow(mainFrame);
	mainFrame->Show(true);
	return true;
}