#include "MainFrame.h"

enum id {
	saveId = wxID_HIGHEST + 1,
	saveAsId = wxID_HIGHEST + 2,
	openId = wxID_HIGHEST + 3,
	ExitId = wxID_HIGHEST + 5,
	SaveBarId = wxID_HIGHEST + 6,
	SaveAsBarId = wxID_HIGHEST + 7,
	AboutId = wxID_HIGHEST + 8,
        OpenBarId
};

MainFrame::MainFrame(const wxString& title, const wxPoint& pos, const wxSize& size)
	: wxFrame(NULL, wxID_ANY, title, pos, size)
{
	//Create menu
	menuBar = new wxMenuBar();
	fileMenu = new wxMenu();
	HelpMenu = new wxMenu();
	HelpMenu->Append(AboutId, "About");
	fileMenu->Append(openId, "Open");
	fileMenu->Append(saveId, "Save");
	fileMenu->Append(saveAsId, "Save As");
	fileMenu->Append(ExitId, "&Exit");
	menuBar->Append(fileMenu, "File");
	menuBar->Append(HelpMenu, "Help");
	SetMenuBar(menuBar);

	toolBar = CreateToolBar();
	wxImage::AddHandler(new wxPNGHandler);
	wxBitmap open(wxT("C:/Users/User/source/repos/Laba6/image/open.png"), wxBITMAP_TYPE_PNG);
	wxBitmap save("C:/Users/User/source/repos/Laba6/image/save-icon-20.png", wxBITMAP_TYPE_PNG);
	wxBitmap save_as("C:/Users/User/source/repos/Laba6/image/save-as-icon.png", wxBITMAP_TYPE_PNG);
	toolBar->AddTool(OpenBarId,"Open",open);
	toolBar->AddTool(SaveBarId, "Save",save );
	toolBar->AddTool(SaveAsBarId, "Save As", save_as);
	toolBar->Realize();

	CreateStatusBar(1);
	SetStatusText("Ready", 0);

	rootSizer = new wxBoxSizer(wxVERTICAL);

	editor = new wxRichTextCtrl(this, wxID_ANY);
	rootSizer->Add(editor, 1, wxEXPAND);

	SetSizer(rootSizer);
}

void MainFrame::OnSaveClick(wxCommandEvent& event)
{
	if (currentFile == NULL)
	{
		wxLogError("File is not specified. Use Save As instead.");
	}
	else {
		if (currentFile->Write(editor->GetValue()))
		{
			SetStatusText("File written");
		}
		else {
			SetStatusText("Failed to write file");
		}
	}
}

void MainFrame::OnSaveAsClick(wxCommandEvent& event)
{
	wxFileDialog
		saveFileDialog(this, _("Save TXT file"), "", "",
			"XYZ files (*.txt)|*.txt", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
	if (saveFileDialog.ShowModal() == wxID_CANCEL)
		return;

	wxFileOutputStream output_stream(saveFileDialog.GetPath());
	if (!output_stream.IsOk())
	{
		wxLogError("Cannot save current contents in file '%s'.", saveFileDialog.GetPath());
		return;
	}
	else
	{
		currentFile = new wxFile(saveFileDialog.GetPath(), wxFile::OpenMode::write);
		if (currentFile->Write(editor->GetValue()))
		{
			SetStatusText("File written");
		}
		else {
			SetStatusText("Failed to write file");
		}
	}
}

void MainFrame::OnOpenClick(wxCommandEvent& event)
{

	wxFileDialog
		openFileDialog(this, _("Open TXT file"), "", "",
			"TXT files (*.txt)|*.txt", wxFD_OPEN | wxFD_FILE_MUST_EXIST);
	if (openFileDialog.ShowModal() == wxID_CANCEL)
		return;

	wxFileInputStream input_stream(openFileDialog.GetPath());
	if (!input_stream.IsOk())
	{
		wxLogError("Cannot open file '%s'.", openFileDialog.GetPath());
		return;
	}

	wxTextFile* file = new wxTextFile(openFileDialog.GetPath());
	file->Open();

	size_t counter = 0;
	size_t lines = file->GetLineCount();

	while (1)
	{
		if (counter == lines)
			break;
		editor->AppendText(file->GetLine(counter));
		counter++;
	}
	currentFile = new wxFile(openFileDialog.GetPath(), wxFile::OpenMode::write);
}
void MainFrame::OnExitClick(wxCommandEvent& event) {
	Close(true);
}

void MainFrame::OnAboutClick(wxCommandEvent& event) {
	AboutFrame* aboutFrame = new AboutFrame("About");
	aboutFrame->Show(true);
}

BEGIN_EVENT_TABLE(MainFrame, wxFrame)
EVT_MENU(saveId, MainFrame::OnSaveClick)
EVT_MENU(saveAsId, MainFrame::OnSaveAsClick)
EVT_MENU(openId, MainFrame::OnOpenClick)
EVT_MENU(ExitId, MainFrame::OnExitClick)
EVT_MENU(AboutId, MainFrame::OnAboutClick)
EVT_TOOL(SaveBarId, MainFrame::OnSaveClick)
EVT_MENU(OpenBarId, MainFrame::OnOpenClick)
EVT_TOOL(SaveAsBarId,MainFrame::OnSaveAsClick)
END_EVENT_TABLE()