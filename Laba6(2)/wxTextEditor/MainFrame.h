#pragma once
#include <wx/wx.h>
#include <iostream>
#include <wx/richtext/richtextctrl.h>
#include <wx/file.h>
#include <wx/filedlg.h>
#include <wx/wfstream.h>
#include <wx/textfile.h>
#include "AboutFrame.h"

using namespace std;

class MainFrame : public wxFrame
{
public:
	MainFrame(const wxString& title, const wxPoint& pos, const wxSize& size);

	wxMenuBar* menuBar;
	wxMenu* fileMenu;
	wxMenu* HelpMenu;
	wxBoxSizer* rootSizer;

	wxRichTextCtrl* editor;

	wxFile* currentFile = NULL;

	wxToolBar* toolBar;

	void OnSaveClick(wxCommandEvent& event);
	void OnSaveAsClick(wxCommandEvent& event);
	void OnOpenClick(wxCommandEvent& event);
	void OnExitClick(wxCommandEvent& event);
	void OnAboutClick(wxCommandEvent& event);

	DECLARE_EVENT_TABLE();

};