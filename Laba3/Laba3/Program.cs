﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GhnomeArray
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = { 2, 7, 5, 8, 35, 545, 6 };
            gnomeSort(array);
            foreach (int a in array)
            {
                Console.Write($"{a}, ");
            }
            Console.ReadKey();
        }

        static void gnomeSort(int[] a)
        {
            int i = 1;
            while (i < a.Length)
            {
                if (i == 0 || a[i - 1] <= a[i])
                    i++;
                else
                {
                    int temp = a[i];
                    a[i] = a[i - 1];
                    a[i - 1] = temp;
                    i--;
                }

            }
        }
    }
}