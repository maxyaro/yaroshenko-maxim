﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BubbleArray
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[5];
            int temp;
            int counter = 0;
            Random random = new Random();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = random.Next(0, 50);
                Console.Write(array[i] + ",");
            }

            for (int i = 0; i < array.Length - 1; i++)
            {
                for (int j = i + 1; j < array.Length - 1; j++)
                {
                    if (array[i] > array[j])
                    {
                        temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;
                    }
                    Console.Write("   " + ++counter);
                }
            }
            Console.WriteLine();
            Console.WriteLine();
            for (int i = 0; i < array.Length; i++)
                Console.Write(array[i] + ",");

            Console.ReadKey();

        }
    }
}